<?php

namespace App\Http\Controllers;

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function index()
    {
      $roles = Role::orderBy('created_at', 'DESC')->get();
      return view('layouts.admin.roles.index')->with('roles', $roles);
    }

    public function create()
    {
      $permissions = Permission::all();
        return view('layouts.admin.roles.create', ['permissions' => $permissions]);
    }

    public function store(Request $request)
    {
      $name = $request->roles;
      $role = new Role();
      $role->name = $name;

      $permissions = $request->permissions;

      $role->save();
    //Looping thru selected permissions
      foreach ($permissions as $permission) {
          $p = Permission::where('id', '=', $permission)->firstOrFail();
       //Fetch the newly created role and assign permission
          $role = Role::where('name', '=', $name)->first();
          $role->givePermissionTo($p);
      }

      return redirect('/roles');
    }

    public function edit($id)
    {
      $role = Role::find($id);
      $permissions = Permission::all();
      return view('layouts.admin.roles.edit', ['role' => $role, 'permissions' => $permissions]);
    }

    public function update(Request $request, $id) {

        $role = Role::findOrFail($id);//Get role with the given id
    //Validate name and permission fields
        $this->validate($request, [
            'name'=>'required|max:10|unique:roles,name,'.$id,
            // 'permissions' =>'required',
        ]);

        $input = $request->except(['permissions']);
        $permissions = $request->permissions;
        $role->fill($input)->save();

        $p_all = Permission::all();//Get all permissions

        foreach ($p_all as $p) {
            $role->revokePermissionTo($p); //Remove all permissions associated with role
        }

        if (!empty($permissions)) {
          foreach ($permissions as $permission) {
              $p = Permission::where('id', '=', $permission)->firstOrFail(); //Get corresponding form //permission in db
              $role->givePermissionTo($p);  //Assign permission to role
          }
        }

        return redirect('/roles');
    }

    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $role->delete();

        return redirect('/roles');

    }


}
