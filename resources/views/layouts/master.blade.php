<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <!-- Ini Halaman Header -->
  <meta charset="utf-8">
  <title>@yield('title')</title>
  <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <!-- VENDOR CSS -->
	<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/vendor/font-awesome/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/linearicons/style.css') }}">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/img/apple-icon.png') }}">
  <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('assets/img/favicon.png') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
  </head>
  <body>
    <!-- NAVBAR -->
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="brand">
    <a href="index.html"><img src="{{ asset('assets/img/logo-dark.png') }}" alt="Klorofil Logo" class="img-responsive logo"></a>
  </div>
  <div class="container-fluid">
    <div class="navbar-btn">
      <button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
    </div>

    <div id="navbar-menu">
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="{{ asset('assets/img/user.png') }}" class="img-circle" alt="Avatar"> <span>{{ Auth::user()->name }}</span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
          <ul class="dropdown-menu">
            <li><a href="#"><i class="lnr lnr-user"></i> <span>My Profile</span></a></li>
            <li><a href="#"><i class="lnr lnr-envelope"></i> <span>Message</span></a></li>
            <li><a href="#"><i class="lnr lnr-cog"></i> <span>Settings</span></a></li>
            <li><a href="{{ route('logout') }}"
               onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();"><i class="lnr lnr-exit"></i> <span>{{ __('Logout') }}</span></a></li>
                             <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                 @csrf
                             </form>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>
<!-- END NAVBAR -->
<div id="wrapper">
  <!-- LEFT SIDEBAR -->
  <div id="sidebar-nav" class="sidebar">
    <div class="sidebar-scroll">
      <nav>
        <ul class="nav">
          <li><a href="{{ url('/dashboard') }}"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
          @can('Otorisasi Admin & Akses ke Halaman Admin')
            <li>
  						<a href="#subPages" data-toggle="collapse" class="collapsed" aria-expanded="false"><i class="lnr lnr-lock"></i> <span>Halaman Admin</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
  						<div id="subPages" class="collapse" aria-expanded="false" style="height: 0px;">
  							<ul class="nav">
                  <li><a href="{{ url('/roles') }}" class=""><i class="lnr lnr-briefcase"></i>Peran</a></li>
  								<li><a href="{{ url('/permissions') }}" class=""><i class="lnr lnr-lock"></i>Izin</a></li>
  								<li><a href="{{ url('/users') }}" class=""><i class="lnr lnr-users"></i>User</a></li>
  							</ul>
  						</div>
  					</li>
          @endcan
            <li><a href="{{ url('/') }}"><i class="lnr lnr-chart-bars"></i> <span>Laporan</span></a></li>
        </ul>
      </nav>
    </div>
  </div>
  <!-- END LEFT SIDEBAR -->
  <div class="main">
    <div class="main-content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            @yield('content')
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
  </body>
  <footer>
    <!-- Ini Halaman Footer -->

<div class="container-fluid">
  <p class="copyright">&copy; 2019 <a href="https://gitlab.com/zyfor90" target="_blank">Febriansyah</a>. All Rights Reserved.</p>
</div>

  <!-- Javascript -->
  <script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
	<script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
	<script src="{{ asset('assets/scripts/klorofil-common.js') }}"></script>

  </footer>
</html>
