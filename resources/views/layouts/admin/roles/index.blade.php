@extends('layouts.master')
​
@section('title')
    Pengelolaan Peran
@endsection
​
@section('content')
  <div class="col-md-3 col-md-offset-9 text-right" style="margin-bottom:15px;">
    <a href="{{ url('/permissions') }}"><button type="button" class="btn btn-default" style="margin:-2.5px;" name="button">Izin</button></a>
    <a href="{{ url('/users') }}"><button type="button" class="btn btn-default" style="margin:-2.5px;" name="button">Pengguna</button></a>
  </div>
  <div class="col-md-12">
  							<!-- TABLE HOVER -->
  							<div class="panel">
  								<div class="panel-heading">
  									<h3 class="panel-title">Panel Peran</h3>
  								</div>
  								<div class="panel-body">
  									<table class="table table-hover">
  										<thead>
  											<tr>
  												<th>#</th>
  												<th>Peran</th>
  												<th>Izin</th>
                          <th>Aksi</th>
  											</tr>
  										</thead>
  										<tbody>
  											@for ($i=1; $i <= $roles->count(); $i++)
                          @foreach ($roles as $role)
                            <tr>
                              <td>{{ $i++ }}</td>
                              <td>{{ $role->name }}</td>
                              <td>{{ str_replace(array('[',']','"'),'', $role->permissions()->pluck('name')) }}</td>
                              <td>
                                {!! Form::model($role, array('url' => array('/roles/'.$role->id.'/delete'), 'method' => 'DELETE')) !!}
                                  {{ csrf_field() }}
                                  <a href="{{ url('/roles/'.$role->id.'/edit') }}"><button type="button" class="btn btn-info" name="button">Edit</button></a>
                                  {!! Form::submit("Hapus", array('class' => 'btn btn-danger')) !!}
                                {!! Form::close() !!}
                              </td>
                            </tr>
                          @endforeach
                        @endfor
  										</tbody>
  									</table>
                    <a href="{{ url('/roles/create') }}"><button type="button" class="btn btn-success" name="button">Tambah Peran</button></a>
  								</div>
  							</div>
  							<!-- END TABLE HOVER -->
  						</div>
@endsection
