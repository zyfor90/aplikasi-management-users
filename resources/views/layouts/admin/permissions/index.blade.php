@extends('layouts.master')
​
@section('title')
    Pengelolaan Izin
@endsection
​
@section('content')
  <div class="col-md-3 col-md-offset-9 text-right" style="margin-bottom:15px;">
    <a href="{{ url('/users') }}"><button type="button" class="btn btn-default" style="margin:-2.5px;" name="button">Pengguna</button></a>
    <a href="{{ url('/roles') }}"><button type="button" class="btn btn-default" style="margin:-2.5px;" name="button">Peran</button></a>
  </div>
  <div class="col-md-12">
  							<!-- TABLE HOVER -->
  							<div class="panel">
  								<div class="panel-heading">
  									<h3 class="panel-title">Panel Peran</h3>
  								</div>
  								<div class="panel-body">
  									<table class="table table-hover">
  										<thead>
  											<tr>
  												<th>#</th>
  												<th>Izin</th>
                          <th>Guard Name</th>
                          <th>Aksi</th>
  											</tr>
  										</thead>
  										<tbody>
  											@for ($i=1; $i <= $permissions->count(); $i++)
                          @foreach ($permissions as $permission)
                            <tr>
                              <td>{{ $i++ }}</td>
                              <td>{{ $permission->name }}</td>
                              <td>{{ $permission->guard_name }}</td>
                              <td>
                                {!! Form::model($permission, array('url' => array('/permissions/'.$permission->id.'/delete'), 'method' => 'DELETE')) !!}
                                  {{ csrf_field() }}
                                  <a href="{{ url('/permissions/'.$permission->id.'/edit') }}"><button type="button" class="btn btn-info" name="button">Edit</button></a>
                                  {!! Form::submit("Hapus", array('class' => 'btn btn-danger')) !!}
                                {!! Form::close() !!}
                              </td>
                            </tr>
                          @endforeach
                        @endfor
  										</tbody>
  									</table>
                    <a href="{{ url('/permissions/create') }}"><button type="button" class="btn btn-success" name="button">Tambah Izin</button></a>
  								</div>
  							</div>
  							<!-- END TABLE HOVER -->
  						</div>
@endsection
