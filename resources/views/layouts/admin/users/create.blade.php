@extends('layouts.master')
​
@section('title')
    Pengelolaan Peran
@endsection
​
@section('content')
  <div class="col-md-3 col-md-offset-9 text-right" style="margin-bottom:15px;">
    <a href="{{ url('/roles') }}"><button type="button" class="btn btn-default" style="margin:-2.5px;" name="button">Peran</button></a>
    <a href="{{ url('/permissions') }}"><button type="button" class="btn btn-default" style="margin:-2.5px;" name="button">Izin</button></a>
  </div>
  <div class="col-md-12">
  							<!-- TABLE HOVER -->
  							<div class="panel">
  								<div class="panel-heading">
  									<h3 class="panel-title">Tambah User</h3>
  								</div>
  								<div class="panel-body">
                    {!! Form::open(array('url' => '/users')) !!}
                      {{ csrf_field() }}
                      <div class="form-group">
                        {!! Form::label("Nama") !!}
                        {!! Form::text("name", '', array('class' => 'form-control', 'placeholder' => 'isi nama disini', 'required')) !!}
                      </div>
                      <div class="form-group">
                        {!! Form::label("Email") !!}
                        {!! Form::email("email", '', array('class' => 'form-control', 'placeholder' => 'isi email disini', 'required')) !!}
                      </div>
                      <div class="form-group">
                        {!! Form::label("Password") !!}
                        {!! Form::password("password", array('class' => 'form-control', 'placeholder' => 'isi password disini', 'required')) !!}
                      </div>
                      <div class="form-group">
                        {!! Form::label("Peran Untuk Pengguna yang akan dibuat :") !!} <br>
                        @foreach ($roles as $role)
                        {!! Form::checkbox("roles[]", $role->id) !!}
                        {!! $role->name !!} <br>
                        @endforeach
                      </div>
                      {!! Form::submit("Tambah", array('class' => 'btn btn-success btn-block')) !!}
                    {!! Form::close() !!}
  								</div>
  							</div>
  							<!-- END TABLE HOVER -->
  						</div>
@endsection
